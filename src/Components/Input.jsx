import React from "react";

const Input = ({ setData, data, handleSubmitClick, error }) => {
  return (
    <>
      <form className="p-4 md:w-full  border-b">
        <label
          htmlFor="name"
          className="text-sm text-slate-600 font-bold"
        >
          NAME*
          <input
            className="bg-white w-full border border-slate-300 rounded py-3 pl-9 pr-3 shadow-sm mb-4"
            id="name"
            type="text"
            onChange={(e) => {
              setData({ ...data, ...{ name: e.target.value } });
            }}
          ></input>
        </label>
        <label
          htmlFor="email"
          className="text-sm text-slate-600 font-bold"
        >
          WORK EMAIL*
          <input
            className="bg-white w-full border border-slate-300 rounded py-3 pl-9 pr-3 shadow-sm mb-4"
            id="email"
            type="email"
            onChange={(e) => {
              setData({ ...data, ...{ email: e.target.value } });
            }}
          ></input>
        </label>
        <span className="text-xs text-red-500">{error}</span>
        <input
          className="bg-indigo-500 text-white py-3 px-8 w-full rounded-xl mt-8 hover:bg-cyan-500 cursor-pointer"
          type="button"
          disabled={!data.name && !data.email}
          value="Add Employee"
          onClick={handleSubmitClick}
        />
      </form>
    </>
  );
};

export default Input;
