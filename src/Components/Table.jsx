import React from "react";

const Table = ({ dataList, setDataList }) => {
//Sorts names in datalist alphabetically and prints them in table cells
    const Data = []
    .concat(dataList)
    .sort((a, b) => a.name > b.name ? 1 : -1)
    .map((data, index) => (
      <tr key={index}>
        <td className="w-1/3 p-2 h-2"  index={index} value={data.name}>
          👤{data.name}
        </td>
        <td className="w-1/3 p-2 h-2"  index={index} value={data.email}>
          {data.email}
        </td>
        <td className="w-1/3 p-2 h-2 text-right">
          <button onClick={() => deleteData(index)}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-6 w-6"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              strokeWidth={2}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
              />
            </svg>
          </button>
        </td>
      </tr>
    ));

  const deleteData = (index) => {
    //Deletes an employee from datalist
    const updatedDataList = [...dataList];
    updatedDataList.splice(index, 1);
    setDataList(updatedDataList);
  };

  return (
    <div className="p-4 w-full md:mx-10 flex flex-col h-full md:pr-16">
      <div className="text-center text-xl my-4">Employees</div>
      <div className="h-full w-full mt-8 ">
        <table className="w-full">
          <thead>
            <tr>
              <th className="w-1/3 bg-slate-200 text-left pr-16">Name</th>
              <th className="w-1/3 bg-slate-200 p-2 text-left pr-16">Email</th>
              <th className="w-1/3 bg-slate-200 pr-16 "></th>
            </tr>
          </thead>
          <tbody>{Data}</tbody>
        </table>
      </div>
    </div>
  );
};

export default Table;

