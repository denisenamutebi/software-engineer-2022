import React, { useState, useEffect } from "react";
import Input from "./Components/Input";
import Table from "./Components/Table";
import isEmail from "validator/lib/isEmail";
import { isAlpha } from "validator";

const App = () => {
  const [dataList, setDataList] = useState([
    //State containg object of all employees
    { name: "Mark", email: "mark@gmail.com" },
    { name: "Rose", email: "rose@gmail.com" },
  ]);
  const [data, setData] = useState({}); //State containing name and email of an employee
  const [error, setError] = useState("");

  useEffect(() => {
    setDataList(JSON.parse(window.localStorage.getItem("dataList")));
  }, []);

  useEffect(() => {
    window.localStorage.setItem("dataList", JSON.stringify(dataList)); //Storing dataList in localStorage
    console.log(localStorage);
  }, [dataList]);

  const handleSubmitClick = () => {
    //Checks if input enters is in correct then saves to state
    if (data.name === "") {
      setError("Fill in name");
    } else if (!isAlpha(data.name)) {
      setError("Use letters only");
    } else if (data.email === "") {
      setError("Fill in email");
    } else if (!isEmail(data.email)) {
      setError("Not an email");
    } else {
      setDataList([...dataList, data]);
      setError("");
    }
    console.log(dataList);
  };

  return (
    <div className="flex justify-center items-center bg-gradient-to-r from-indigo-500 min-h-screen to-cyan-500 py-10 pb-10">
      <div className="bg-white rounded m-0 w-full mx-4  mt-12 md:w-4/5 h-full">
        <div className='bg-slate-200 p-8 text-center text-3xl font-bold'>Employee list</div>
        <div className='flex flex-col h-full p-8'>
        <Input setData={setData} data={data} handleSubmitClick={handleSubmitClick} error={error}/> 
        <Table dataList={dataList} setDataList={setDataList} />
        </div>
      </div>
    </div>
  );
};

export default App;
